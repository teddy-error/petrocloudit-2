import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import User from './User';
import { UserService } from './user.service';

@Injectable()
export class UsersResolver implements Resolve<User[]> {
  constructor(private service: UserService) {}

  resolve(route: ActivatedRouteSnapshot): Promise<User[]> {
    return this.service.getUsers();
  }
}
