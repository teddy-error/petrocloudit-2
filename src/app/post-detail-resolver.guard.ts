import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import Post from './Post';
import { PostService } from './posts.service';

@Injectable()
export class PostDetailResolver implements Resolve<Post> {
  constructor(private service: PostService) {}

  resolve(route: ActivatedRouteSnapshot): Promise<Post> {
    const id = route.paramMap.get('id');
    return this.service.getPost(+id);
  }
}
