import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PostListComponent } from './post-list/post-list.component';

import { PostListResolver } from './post-list-resolver.guard';
import { PreloadAllModules } from '@angular/router/';
import { PostDetailComponent } from './post-detail/post-detail.component';
import { PostDetailResolver } from './post-detail-resolver.guard';
import { CommentsResolver } from './comments-resolver.guard';
import { UsersResolver } from './users-resolver.guard';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  {
    path: '',
    component: PostListComponent,
    resolve: {
      posts: PostListResolver
    }
  },
  {
    path: 'p/:id',
    component: PostDetailComponent,
    resolve: {
      post: PostDetailResolver,
      comments: CommentsResolver,
      users: UsersResolver
    }
  },
  {
    path: 'u/:id',
    component: UserComponent,
    resolve: {
      users: UsersResolver,
      posts: PostListResolver
    }
  }
];
@NgModule({
  imports: [
    RouterModule.forRoot(
      routes,
      {
        // enableTracing: true, // debug
        preloadingStrategy: PreloadAllModules
      }
    )
  ],
  exports: [
    RouterModule,
  ],
  providers: [
    PostListResolver,
    PostDetailResolver,
    CommentsResolver,
    UsersResolver,
  ]
})
export class AppRoutingModule { }
