import { Component, OnInit } from '@angular/core';
import User from '../User';
import { ActivatedRoute } from '@angular/router';
import Post from '../Post';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {
  user: User;
  posts: Post[];

  constructor(private route: ActivatedRoute) { }

  ngOnInit() {
    this.route.data.subscribe((data: {
      users: User[];
      posts: Post[];
    }) => {
      const id = +this.route.snapshot.paramMap.get('id');
      this.user = data.users.find((u) => u.id === id);
      this.posts = data.posts.filter(p => p.userId === id);
    });
  }

}
