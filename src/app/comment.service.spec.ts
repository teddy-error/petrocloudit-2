import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import Comment from './Comment';
import { CommentService } from './comment.service';
import { mockComments } from './mockComments';

describe('CommentsService', () => {
  let mocks: Comment[];
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [ CommentService ],
    });
    mocks = mockComments;

  });

  afterEach(() => {
    //
  });

  it('should return comments for a given post', async(() => {
    const service = TestBed.get(CommentService);
    const http = TestBed.get(HttpTestingController);

    const randomPostId = Math.round(Math.random() * 100);
    const expectedComments = mocks.filter((comment) => {
      return comment.postId === randomPostId;
    });

    let comments;
    service.get(randomPostId).then((c) => {
      comments = c;
      expect(comments).toEqual(expectedComments);
    });
    http.expectOne(`https://jsonplaceholder.typicode.com/comments?postId=${randomPostId}`).flush(expectedComments);
  }));
});
