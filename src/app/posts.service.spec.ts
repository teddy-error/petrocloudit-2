import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import Post from './Post';
import { PostService } from './posts.service';
import { mockPosts } from './mockPosts';

describe('PostsService', () => {
  let mocks: Post[];
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [ PostService ],
    });
    mocks = mockPosts;

  });

  afterEach(() => {
    //
  });

  it('should get and return an array of posts', async(() => {
    const service = TestBed.get(PostService);
    const http = TestBed.get(HttpTestingController);
    const expectedPosts = mocks.slice(0, 10);
    let posts;
    service.getPosts().then((p) => {
      posts = p;
      expect(posts).toEqual(expectedPosts);
    });
    http.expectOne('https://jsonplaceholder.typicode.com/posts').flush(expectedPosts);
  }));
});
