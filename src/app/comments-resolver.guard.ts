import { Injectable } from '@angular/core';
import { Router, Resolve, ActivatedRouteSnapshot } from '@angular/router';

import Comment from './Comment';
import { CommentService } from './comment.service';

@Injectable()
export class CommentsResolver implements Resolve<Comment[]> {
  constructor(private service: CommentService) {}

  resolve(route: ActivatedRouteSnapshot): Promise<Comment[]> {
    const id = route.paramMap.get('id');
    return this.service.get(+id);
  }
}
