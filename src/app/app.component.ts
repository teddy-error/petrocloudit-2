import { Component, OnInit } from '@angular/core';
import { PostService } from './posts.service';
import Post from './Post';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'app';
  posts: Post[];
  constructor(private postService: PostService) {}

  async ngOnInit() {
    this.posts = await this.postService.getPosts();
  }

}
