import { Component, OnInit } from '@angular/core';
import Post from '../Post';
import { Router, ActivatedRoute  } from '@angular/router';

@Component({
  selector: 'app-post-list',
  templateUrl: './post-list.component.html',
  styleUrls: ['./post-list.component.css']
})
export class PostListComponent implements OnInit {
  posts: Post[];

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.route.data.subscribe((data: { posts: Post[] }) => {
      this.posts = data.posts;
    });
  }

}
