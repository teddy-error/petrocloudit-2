import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import User from './User';

@Injectable()
export class UserService {
  URI = 'https://jsonplaceholder.typicode.com/users';
  users: User[] = [];

  constructor(private httpClient: HttpClient) {}

  private async fetch(): Promise<User[]> {
    return this.httpClient.get<User[]>(this.URI).toPromise();
  }

  async getUsers() {
    if (!this.users.length) {
      this.users = await this.fetch();
    }
    return this.users;
  }

}
