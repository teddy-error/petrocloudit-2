import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import Post from './Post';

@Injectable()
export class PostService {
  URI = 'https://jsonplaceholder.typicode.com/posts';
  posts: Post[] = [];

  constructor(private httpClient: HttpClient) {}

  private async fetch(): Promise<Post[]> {
    return this.httpClient.get<Post[]>(this.URI).toPromise();
  }

  private findPost(postId: number) {
    const result = this.posts.find((post) => {
      return post.id === postId;
    });
    return result;
  }

  async getPosts() {
    if (!this.posts.length) {
      this.posts = await this.fetch();
    }
    return this.posts;
  }

  async getPost(postId: number) {
    let post = this.findPost(postId);
    if (!post) {
      this.posts = await this.fetch();
      post = await this.getPost(postId);
    }
    return post;
  }
}
