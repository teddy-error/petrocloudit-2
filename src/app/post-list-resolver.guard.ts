import { Injectable } from '@angular/core';
import { Router, Resolve, RouterStateSnapshot, ActivatedRouteSnapshot } from '@angular/router';
import { Observable } from 'rxjs/Observable';

import Post from './Post';
import { PostService } from './posts.service';

@Injectable()
export class PostListResolver implements Resolve<Post[]> {
  constructor(private service: PostService) {}

  resolve(): Promise<Post[]> {
    return this.service.getPosts();
  }
}
