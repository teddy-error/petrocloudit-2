import { TestBed, async } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import User from './User';
import { mockUsers } from './mockUsers';
import { UserService } from './user.service';

describe('UserService', () => {
  let mocks: User[];
  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [
        HttpClientTestingModule,
      ],
      providers: [ UserService ],
    });
    mocks = mockUsers;

  });

  afterEach(() => {
    //
  });

  it('should get and return an array of users', async(() => {
    const service = TestBed.get(UserService);
    const http = TestBed.get(HttpTestingController);
    const expectedUsers = mocks;
    let users;
    service.getUsers().then((u) => {
      users = u;
      expect(users).toEqual(expectedUsers);
    });
    http.expectOne('https://jsonplaceholder.typicode.com/users').flush(expectedUsers); // .flush(mockPosts);
  }));
});
