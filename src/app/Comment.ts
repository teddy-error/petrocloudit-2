export default class Comment {
  postId: number;
  id: number;
  userId?: number;
  name: string;
  email: string;
  body: string;
}
