import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import Post from '../Post';
import Comment from '../Comment';
import User from '../User';

@Component({
  selector: 'app-post-detail',
  templateUrl: './post-detail.component.html',
  styleUrls: ['./post-detail.component.css']
})
export class PostDetailComponent implements OnInit {
  post: Post;
  comments: Comment[];
  users: User[];

  constructor(private route: ActivatedRoute) {}

  ngOnInit() {
    this.getPost();
  }

  user(userId: number): User {
    return this.users.find((u) => {
      return u.id === userId;
    });
  }

  getPost(): void {
    this.route.data.subscribe((data: {
      post: Post
      comments: Comment[]
      users: User[]
    }) => {
      this.post = data.post;
      this.comments = data.comments;
      this.users = data.users;
    });
  }

}
