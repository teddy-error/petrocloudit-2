export default class Post {
  body: string;
  id: number;
  title: string;
  userId: number;
}
