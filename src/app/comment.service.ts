import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import Comment from './Comment';

@Injectable()
export class CommentService {
  URI = 'https://jsonplaceholder.typicode.com/comments';

  constructor(private http: HttpClient) {}

  async get(postId: number) {
    return this.http
      .get<Comment[]>(`${this.URI}?postId=${postId}`)
      .toPromise()
      // Provide valid relation to users
      .then((comments) => {
        return comments.map((c) => {
          const randomId = Math.floor(Math.random() * 9) + 1;
          c.userId = randomId;
          return c;
        });
    });
  }

}
